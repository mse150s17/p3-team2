import matplotlib.pyplot as plt
import numpy as py

#lnD=lnA+(-Ea/k)(1/T).

#makes array of ln(D) values.
lnD = py.array([py.log(7.48E-16), py.log(2.78E-14), py.log(1.66E-13)])

#makes array of (1/T) values.
invT = py.array([1/1010, 1/1190, 1/1305])

#extracting the slope of lnD vs 1/T, as a 1st degree polynomial.
slope, intercept = py.polyfit(lnD, invT, 1)

#known value of the Boltzmann constant (in m^2*kg*s-2*K-1)
k = 1.38064852E-23

#slope = (-Ea/k) so Ea = slope*-k.
Ea = slope*-k
print("The activation energy is %.4E" % Ea, "Joules.")

#intercept = lnA so A = exp(intercept).
A = py.exp(intercept)

#prints calculated value of A
print("The constant A is %.5f" % A)
