##README##

*	This repository is for the calculation of activation energy based on the equation for 
	diffusivity and from collected data in diffusivity.txt and to find the value of Planck's 
	constant extracted from x-ray data.  This is done using two programs in python: diffplot.py and xplot.py.  

###What does this Repository do?###

*	diffplot.py:
	*	 Makes an array of natural log values for diffusivity
	*	 Makes an Array of 1/T for given temps
	*	 Uses py.polyfit to apply a linear fit to data
	*	 Assigns variable k as Boltzmann's constant
	*	 Solves for activation energy using slope and k, and A using intercept

*	xplot.py:
	*	 Makes an array of given wavelengths and atomic numbers
	* 	 Defines Frequency as the speed of light divided by wavelength
	*	 Plots atomic number vs. sqrt(frequency)
	*	 Uses py.polyfit to linearly fit data and extracts the intercept and slope
	*	 Using stats.linregress finds r^2 value (how well the line fits)
	*	 Chooses a good model, plots this good model
	*	 Calculates the difference between experimental h and actual h

### How do I use the program? ###

*	With diffusivity.txt supplied (diffusivity in cm x cm/s, Temperature in Kelvin (K)), the 
        program diffplot.py will calculate activation energy and the constant A from the diffusivity 
        equation, then print the result to the user.
*	To use the diffplot program, enter "python diffplot.py" in your terminal.
*	To use the xplot program, enter "python xplot.py" in your terminal.
###Contribution guidelines###

*	Writing tests: Nick added a test commit from his laptop.
*	Code review
*	Other guidelines

###Who do I talk to?###

*    	Please see contributors.txt file. You may contact any of the members listed in that file for 
	questions on this project.

