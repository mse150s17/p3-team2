import numpy as py
import matplotlib.pyplot as plt
import cv2

img = cv2.imread('grain.png')
from pyparty import Canvas, MultiCanvas

BG  = 'https://raw.github.com/hugadams...'
REZ = (1024, 1024)  #Image resolution
RAD = (12, 18)      #Radius range (px)
COLOR = (200, 255)  #Color range
NOISE = 0.10        #Percent noise

#*The Canvas crops the image to our desired resolution upon initialization. Next, randomly-sized ellipses are added, and the Canvas grid is set to 20 pixels per division; this controls the inter-particle spacing.

from random import randint as R_int

cnvs = Canvas(rez=REZ, background=BG)
cnvs.grid.div = 20 #20x20 grid

for (cx, cy) in cnvs.gpairs('centers'):
        cnvs.add('ellipse',
                center = (cx,cy),
                xradius = R_int(*RAD),
                yradius = R_int(*RAD),
                phi = R_int(0, 360),
                color = R_int(*COLOR) )
					
cnvs.show(annotate=True)

from skimage.filter import gaussian_filter
from pyparty.noise import pepper
from pyparty.plots import zoom
					
smooth = gaussian_filter(cnvs.image, 3)
noisy = pepper(smooth, NOISE)
zoom(noisy, (0, 0, 250, 250))

from pyparty.plots import showim, splot
from pyparty.data import nanolabels
					
NANOLABELS = nanolabels()
NAMES = ('singles', 'dimers', 'trimers', 'clusters')
showim(NANOLABELS, 'spectral', title='Labeled Nanoparticles')
					
mc = MultiCanvas.from_labeled(nanolabels(), *NAMES)
mc.set_colors('r', 'g', 'y', 'magenta')
mc.show(names = True, ncols=2, figsize=(7,5))
					
ax1, ax2 = splot(1,2)
mc.hist(ax1, attr='eccentricity', bins=30)
mc.pie(ax2, attr='area', explode=(0,0,0, 0.1))







