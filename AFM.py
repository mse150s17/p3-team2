import numpy as np
import scipy as sci
import scipy.ndimage 
import matplotlib.image as image
import matplotlib.pyplot as plt
from PIL import Image
from scipy import misc

#Import image to be read
img = image.imread('dna.tif')

#Command to save file from .tif to .png extension
scipy.misc.imsave('dna.png', img)

#Command to read and flatten the image
flatten = misc.imread('dna.png', flatten=True)

#Function to show image
plt.imshow(img)

#A series of for loops to calculate the number of pixels in a dna triangle
pixtri = 0
for i in range(150):
	for j in range(130):
		if flatten[i+2720, j+400] > 90:
			pixtri += 1

#Prints out the number of pixels in a dna triangle
print('The amount of pixels per triangle is ' + str(pixtri))

#A series of for loops to calculate the number of pixels in the entire image
totalpix = 0
for i in range(2956):
	for j in range(2960):
		if flatten[i,j] > 90:
			totalpix += 1

#Prints out the total number of pixels in the image
print('The total pixel count is ' + str(totalpix))

#Calculates the total number of dna triangles in the image
totaltri = totalpix/pixtri

#Prints out the total number of dna triangles in the image
print('The number of triangles is ' + str(totaltri))











