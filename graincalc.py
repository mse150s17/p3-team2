import numpy as py
import matplotlib.pyplot as plt
import matplotlib.image as mpim
import scipy.misc as misc


#reads an image as an array of the red, green, and blue values at each pixel
img = mpim.imread('grain.png')

#separates red, green, and blue
R = img[:,:,0]

G = img[:,:,1]

B = img[:,:,2]

gray = R*0.2989 + G*0.5870 + B*0.1140 

#converts to a greyscale image

plt.imshow(gray, cmap = plt.get_cmap('gray'))
plt.show()

print(gray)

grain_count = 0

#0.0 = black, 1.0 = white, setting threshold at 0.4.

for j in range(1700):
	for i in range(2070):
		if img[i,j] > 0.4
			grain_count += 1

area = 1.33*10**-6

#prints out how many grains per square meter
print(str(grain_count)/area) + ' grains per square meter')

