import matplotlib.pyplot as plt
import numpy as py
import sys

#Program for Challenge 2.
from scipy import stats

#Array for the wavelengths of given various elements
wavelength = py.array([0.987, 0.834, 0.573, 0.335, 0.229, 0.194, 0.154, 0.093, 0.021])

#Array for the atomic number of each element.
atomicnum = py.array([12, 13, 16, 20, 24, 26, 29, 37, 74])

#Frequency calculation
frequency = 3.0E8 / wavelength

#PART A: plotting.
plt.plot(atomicnum, py.sqrt(frequency))
plt.xlabel('Atomic Number')
plt.ylabel('Square Root of the Frequency')
plt.title("'Bad' Model")
plt.savefig("badplot.png")


#PART B: extracting the slope and intercept.
slope, intercept = py.polyfit(atomicnum, py.sqrt(frequency), 1)


#Assignment of variable B.
B = slope

#Prints variable B.
print('B is equal to %.4f' % B ,'which is the slope of the plot of the square root of frequency vs atomic number')

#Assignment of variable C.
C = intercept/-B

#Prints variable C.
print('C is equal to %.4f' % C)

#PART C: finds r^2 value
slope, intercept, r_value, p_value, std_err = stats.linregress(atomicnum, py.sqrt(frequency))
print("The r-squared value of the linear fit is %.4f" % r_value**2)


#PART D: defining the 'good' model
#Planck's constant defined
h_theoretical = 6.626E-34 

freq = (13.6*3*(atomicnum-1)**2)/(4*h_theoretical)

#plotting
plt.plot(atomicnum, py.sqrt(freq))
plt.xlabel('Atomic Number')
plt.ylabel('Square Root of the Frequency')
plt.title("'Good' Model")
plt.savefig("goodplot.png")

#Calculating Planck's constant from our experimental data
h_experimental = (13.6*3*(atomicnum-1)**2)/(4*frequency)

#Comparing the experimental to the theoretical
h_error = (h_experimental-h_theoretical)/h_theoretical

#Print results.
print("The error in the 'Good' Model is %.4E" % py.average(h_error))






